<?php
	function test() {
		echo "<em>" . "<span>" . "Resultat de code:" . "</em>" . "</span>" . "<br>";
    	$foo = "Mangue";

	echo  $foo . "\n". 'est un fruit'. $GLOBALS["foo"]."\n";
    	echo 'Consommer le ';
	}

	$foo = "\n". "très saine et a un goût exotique !!";
	test();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>SUPER GLOBALES</title>
	<meta charset="utf_8">
	<style> 

	body {
		border: 1px solid black;
		border-color: #A52A2A;
		padding: 20px 20px 20px 20px;
		margin: 100px 300px 20px 200px;
		background-color: #F5F5DC;
		}
	h1 	{
		color: #A52A2A;
	}
	span {
		color: #A52A2A;
		}

	</style> 	
    </head>
   
    <body>

	<div> 
	<h1> $GLOBALS </h1>
	<p> <span> $GLOBALS </span> est une variable qui nous permet d'avoir acces dans toute la page y compris script (aussi dans la function et method). C'est un tableau qui index les variables globales. </p> 
	<p> Aussi quand on utilise dans un function une variable est local de nature, si on mets global au debut il va le mettre dans <span> $GLOBALS.</span> </p>  
    
    <div> 
    <h3> CODE PHP:</h3>
    <p> function test() { </p> 
    <p>	$foo = "Mangue";  </p> 

	<p> echo  $foo . "\n". 'est un fruit'. $GLOBALS["foo"]."\n"; </p> 
    <p> echo 'Consommer le '; </p> 
	<p> } </p> 

	<p> $foo = "\n". "très saine et a un goût exotique !!"; </p>
	<p> test(); </p> 
    </div>  
	</div>
 	
	</body>

</html>


