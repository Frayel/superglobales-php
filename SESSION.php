<?php
   session_start();
   
   if( isset( $_SESSION['counter'] ) ) {
      $_SESSION['counter'] += 1;
   }else {
      $_SESSION['counter'] = 1;
   }
   $msg = "You have visited this page ".  $_SESSION['counter'];
   $msg .= "in this session.";
?>

<!DOCTYPE html>
<html>
    <head>
        <title>SUPER GLOBALES</title>
	<meta charset="utf_8">

	<style> 
		body {
			padding: 20px 70px 20px 20px;
			margin: 100px 300px 20px 200px;
			background-color: #F5F5DC;
			border: 1px solid black;
			border-color: #A52A2A;
		}

		h1 	{
		color: #A52A2A;
		}

		span {
		color: #A52A2A;
		}
	</style> 	
  
	</head>
   
  <body>

	<p> <em> <span> Resultat de code:</em> </span> </p> 
    <?php  echo ( $msg ); ?>  

	<div> 
		<h1> $_SESSION </h1>
		<p> <span> $_SESSION </span>:Une autre façon de rendre les données accessibles sur les différentes pages d'un site Web entier consiste à utiliser une session PHP. </p>
        <p> Une session crée un fichier dans un répertoire temporaire sur le serveur où les variables de session enregistrées et leurs valeurs sont stockées. Ces données seront disponibles sur toutes les pages du site lors de cette visite.</p> 
	<div> 

	<h3> CODE PHP:</h3>
   
    <p> session_start(); </p> 
   
    <p> if( isset( $_SESSION['counter'] ) ) { </p>
    <p> $_SESSION['counter'] += 1; </p>
    <p> }else { </p>
    <p> $_SESSION['counter'] = 1; </p>
    <p> } </p> 
	
    <p> $msg = "You have visited this page ".  $_SESSION['counter']; </p>
    <p> $msg .= "in this session."; </p>
    <p> ?> </p>
	</div>

	</body>
</html>
