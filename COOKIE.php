<?php
$cookie_name = "user";
$cookie_value = "Yeliz";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
?>


<!DOCTYPE html>
<html>
    <head>
        <title>SUPER GLOBALES</title>
	<meta charset="utf_8">

	<style> 
		body {
			padding: 20px 70px 20px 20px;
			margin: 100px 300px 20px 200px;
			background-color: #F5F5DC;
			border: 1px solid black;
			border-color: #A52A2A;
		}

		h1 	{
		color: #A52A2A;
		}

		span {
		color: #A52A2A;
		}
	</style> 	
  
	</head>
   
  <body>

	<?php
	if(!isset($_COOKIE[$cookie_name])) {
    echo "Cookie named '" . $cookie_name . "' is not set!";
	} else {
    echo "Cookie '" . $cookie_name . "' is set!<br>";
    echo "Value is: " . $_COOKIE[$cookie_name];
	}
	?>

	<div> 
	<h1> $_COOKIE </h1>
	<p> <span> $_COOKIE</span>: Un cookie est souvent utilisé pour identifier un utilisateur. 
	Un cookie est un petit fichier que le serveur intègre sur l'ordinateur de l'utilisateur. 
	Chaque fois que le même ordinateur demande une page avec un navigateur, 
	il envoie également le cookie. PHP nous permets de créer et récupérer des valeurs de cookies.</p>

	<div> 


    <h3> CODE PHP:</h3>
		<p> <em> <span> Au debut de la page </span> </em>  </p> 
		<p> $cookie_name = "user"; </p> 
		<p> $cookie_value = "Yeliz"; </p> 
		<p> setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day </p> 

		<p> <em> <span> Dans la partie body </span> </em> </p> 
		<p> if(!isset($_COOKIE[$cookie_name])) { </p>
    <p> echo "Cookie named '" . $cookie_name . "' is not set!"; </p> 
		<p> } else { </p> 
    <p> echo "Cookie '" . $cookie_name . "' is set!<br>"; </p>
    <p> echo "Value is: " . $_COOKIE[$cookie_name]; </p>
		<p> } </p>
	</div>

	</body>
</html>
